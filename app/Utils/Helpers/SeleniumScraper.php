<?php

namespace App\Utils\Helpers; 

use Facebook\WebDriver\Firefox\FirefoxProfile;
use Facebook\WebDriver\Firefox\FirefoxDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;


class SeleniumScraper 
{
    private $cats = [];
    private $products = [];
    private $pages = [];
    private $driver = null;
    private $product_treshhold = 5; //100
    private $cat_id = 1;
    private $sub_cat_id = 1;
    private $product_id = 1;

    private $cat_file = null;
    private $sub_cat_file = null;
    private $product_file = null;

    private $current_cat = null;
    private $current_sub_cat = null;

    public function __construct()
    {
        
        $this->cat_file = fopen('categories.csv', 'w+');
        fwrite($this->cat_file, 'id,name');
        

        $this->sub_cat_file = fopen('sub_categories.csv', 'w+');
        fwrite($this->sub_cat_file, 'id,name,category');
        

        
        $this->products_file = fopen('products.csv', 'w+');
        fwrite($this->products_file, 'name,price,image,description,category,sub_category');// for original products.
        // fwrite($this->products_file, 'id,name,category,sub_category');// for testing cat_with _sub_cat products.

        

        self::mainCategory();
        
        fclose($this->cat_file);
        fclose($this->sub_cat_file);
        fclose($this->products_file);
        
    }
    
    private function mainCategory()
    {
        
        ini_set('max_execution_time', 0);
        $homePage_url = "https://www.alibaba.com/Products?spm=a2700.details.scGlobalHomeHeader.706.579a52cadBekuN";
        $waitSeconds = 25;  
        $host = 'http://localhost:4444/wd/hub'; 
        
        $capabilities = DesiredCapabilities::chrome();
        $this->driver = RemoteWebDriver::create($host, $capabilities, 60000, 60000);

        $this->driver->get($homePage_url);
        $this->driver->executeScript('window.scrollTo(0,document.body.scrollHeight)');

        $categories = ($this->driver->findElements(WebDriverBy::cssSelector('.sub-item')));
       
        foreach($categories as $key => $category)
        {
            $title          = trim(explode('(',$category->findElement(WebDriverBy::cssSelector('.sub-title'))->getText())[0]);
            $sub_categories = $category->findElements(WebDriverBy::cssSelector('.sub-item-cont li'));
            
            // $categoriesFile = fopen('categories.csv', 'a+');
            fwrite($this->cat_file, PHP_EOL  . $this->cat_id . ',' . $title);
            // fclose($categoriesFile);
            $this->current_cat = $title;
            $this->cat_id++;


            $array = [];

            $sub_cats = [];
            

            dump('Searching .. ' . $title);

            foreach($sub_categories as $sub_category_key => $sub_category){
                $a_tag      = $sub_category->findElement(WebDriverBy::tagName('a'));
                $link_text  = $a_tag->getText();
                $link       = $a_tag->getAttribute('href');

                // $subcategoriesFile = fopen('sub_categories.csv', 'a+');
                fwrite($this->sub_cat_file, PHP_EOL  . $this->sub_cat_id . ',' . $link_text . ',' . $this->current_cat);
                // fclose($subcategoriesFile);
                $this->current_sub_cat = $link_text;
                $this->sub_cat_id++;

                self::initSearch($link);
                // self::fetchNextPageData();
                // self::updateProductData();

                $data2 = [
                    'title'     => $link_text,
                    'url'       => $link,
                    'products'  => $this->products
                ];


                dump('Sub_Cat.. For  .. ' . $title . ' .... ' . $link_text);
                array_push($sub_cats, $data2);

                $this->driver->quit();
                
                $this->driver->quit();
            }

            $data = [
                'title'      => $title,
                'categories' => $sub_cats,
            ];

            array_push($this->cats, $data);
        }

      
        
        $this->driver->quit();
        
        $this->driver->quit();
    }

    

    private function fetchUrl($url){
        ini_set('max_execution_time', 0);
        // header("Content-Type: text/html; charset=UTF-8");   
        $waitSeconds = 25;  
        $host = 'http://localhost:4444/wd/hub'; 
        
        $capabilities = DesiredCapabilities::chrome();
        $this->driver = RemoteWebDriver::create($host, $capabilities, 60000, 60000);

        $this->driver->get($url);
        $this->driver->executeScript('window.scrollTo(0,document.body.scrollHeight)');
    }

    private function initSearch($url){
        self::fetchUrl($url);

        $pages = $this->driver->findElements(WebDriverBy::cssSelector('.seb-pagination__pages a'));
        $products = $this->driver->findElements(WebDriverBy::cssSelector('.J-offer-wrapper'));

        foreach ($pages as $key => $pg) {
            if($key < 1){
                $link_url = $pg->getAttribute('href');
                if($link_url){
                    $this->pages[] = $link_url;
                }  
            }
        }

        foreach ($products as $key => $pro) 
        {
            $data_params = explode('&',$pro->getAttribute('data-params'));
            $data = [
                'p_id' =>  $this->product_id,
                'id'    => explode('=', $data_params[2])[1],
                'url'   => $pro->findElement(WebDriverBy::cssSelector('a.organic-gallery-offer__img-section'))->getAttribute('href'),
                'image' =>  [
                    'path'  => $pro->findElement(WebDriverBy::tagName('img'))->getAttribute('src'),
                    'name'  => $pro->findElement(WebDriverBy::tagName('img'))->getAttribute('alt')
                ],
                'name'          =>  $pro->findElement(WebDriverBy::tagName('h4'))->getText(),
                'category'      =>  $this->current_cat,
                'sub_category'  =>  $this->current_sub_cat,
                'price' =>  $pro->findElement(WebDriverBy::cssSelector('p.elements-offer-price-normal'))->getText(),
            ];
            $name = str_replace(',' ,' ' ,$data['name']);
            // $productsFile = fopen('products.csv', 'a+');
            // fwrite($this->products_file, PHP_EOL  . $this->product_id . ',' . $name .  ',' . $this->current_cat . ',' . $this->current_sub_cat);
            // fclose($productsFile);
            $this->product_id++;
            

            $this->products[] = $data;
            

        }
        
        $this->driver->quit();
    }

    private function fetchNextPageData(){
        foreach ($this->pages as $page_index => $next_page_link) {
            
            // if(count($this->products) <= 40){
            //     break;
            // }
            
            self::fetchUrl($next_page_link);

            $products = $this->driver->findElements(WebDriverBy::cssSelector('.J-offer-wrapper'));

            foreach ($products as $key => $pro) {
                $data_params = explode('&',$pro->getAttribute('data-params'));
                $data = [
                    'id'    => explode('=', $data_params[2])[1],
                    'url'   => $pro->findElement(WebDriverBy::cssSelector('a.organic-gallery-offer__img-section'))->getAttribute('href'),
                    
                ];
            
                $this->products[] = $data;
            }
            
            $this->driver->quit();

            // if(count($this->products) >= $this->product_treshhold){
            //     break;
            // }

        }
    }

    private function updateProductData()
    {

        foreach ($this->products as $single_product_index => $single_product) 
        {
            if($single_product_index < 2)
            {
                $single_product_link = $single_product['url'];
            self::fetchUrl($single_product_link);

            $title = $this->driver->findElement(WebDriverBy::id('module_title'))->getText();
            $price = $this->driver->findElement(WebDriverBy::cssSelector('.ma-price-wrap'))->getText();
            $images = $this->driver->findElements(WebDriverBy::cssSelector('.main-image-thumb-ul img'));
            $description = $this->driver->findElement(WebDriverBy::cssSelector('#J-rich-text-description'))->getText();
            $price = str_replace(',', ' ', $price);
            $price = preg_replace("/\r|\n/", " ", $price);
            $description = str_replace('\r\n', ' ', str_replace(',',' ', $description));
            $description = preg_replace("/\r|\n/", " ", $description);
            
            dump($price);
            $loc_images = [];
            
            foreach ($images as $image_index => $image) 
            {
                $loc_images[] = $image->getAttribute('src');
            }
            
            $this->products[$single_product_index]['title'] =  $title;
            $this->products[$single_product_index]['price'] =  $price;
            $this->products[$single_product_index]['image'] =  $loc_images;
            $this->products[$single_product_index]['description'] =  $description;
            dump($loc_images[0]);
            
            fwrite($this->products_file , PHP_EOL . $title.',' .$price. ',' .$loc_images[0]. ','  .$description.','. $this->products[$single_product_index]['category'] . ',' .$this->products[$single_product_index]['sub_category']);

            $this->driver->quit();
            }
            
            
        }
    }
}



