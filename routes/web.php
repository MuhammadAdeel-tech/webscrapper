<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\scraperController;
use App\Http\Controllers\seleniumController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/seleniumScrap', [seleniumController::class, 'scraping']);
Route::get('/fileWrite', [scraperController::class, 'writeFile']);
Route::get('/gouteScraper', [scraperController::class, 'index']);





